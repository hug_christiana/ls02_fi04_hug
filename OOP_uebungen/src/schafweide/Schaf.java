package schafweide;

public class Schaf
{

	private String name;
	private byte alter;
	private float wollMenge;
	// Konstruktor 1

	public Schaf()
	{
		this.name = "unnamed";
		this.alter = 0;
		this.wollMenge = 0;
		System.out.println("Im parameterlosen Konstruktor 1.");
	}

	// Konstruktor 2
	public Schaf(String name)
	{
		this.alter = 0;
		this.wollMenge = 0;
		this.name = name;
		System.out.println("Im Konstruktor 2.");
	}

	// Konstruktor 3
	public Schaf(String name, byte alter, float wolle)
	{
		this(name); 
		this.alter = alter;

		this.wollMenge = wolle;
		System.out.println("Im Konstruktor 3.");
	}
	
	//Methoden
	
	//Getter und Setter
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setAlter(byte alter) {
		this.alter = alter;
	}
	
	public float getAlter() {
		return this.alter;
	}
	
	public void setWollMenge(float wolle) {
		this.wollMenge = wolle;
	}
	
	public float getWollMenge() {
		return this.wollMenge;
	}
	
	public void merkmaleAusgeben(String objektName) {
		System.out.println("\n- Ausgabe der Merkmale von " + objektName + " -");
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter + " Jahre");
		System.out.println("Wollmenge: " + wollMenge + " m^2");
	}
	
}

