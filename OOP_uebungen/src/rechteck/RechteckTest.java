package rechteck;

import java.util.Scanner;

public class RechteckTest
{
	
public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print ("Seite A:");
		double seiteA = myScanner.nextDouble();
		
		System.out.print ("Seite B:");
		double seiteB = myScanner.nextDouble();
		
		Rechteck square = new Rechteck(seiteA, seiteB);
        System.out.println("Fl�che: " + square.getFlaeche());
        System.out.println("Umfang: " + square.getUmfang());
        System.out.println("Diagonale: " + square.getDiagonale());
		
	}

}
