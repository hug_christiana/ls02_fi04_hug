package rechteck;

import java.awt.Point;

public class Rechteck 
{

	private double seiteA;
	private double seiteB;
	private Point mittelpunkt;

	public Rechteck () 
	{
		setSeiteA(0);
		setSeiteB(0);
	}

	public Rechteck (double seiteA, double seiteB) 
	{
		setSeiteA(seiteA);
		setSeiteB(seiteB);
	}

	public void setSeiteA(double seiteA) 
	{
		if(seiteA > 0)
			this.seiteA = seiteA;
		else
			this.seiteA = 0;
	}

	public double getSeiteA() 
	{
		return this.seiteA;
	}

	public void setSeiteB(double seiteB) 
	{
		if(seiteB > 0)
			this.seiteB = seiteB;
		else
			this.seiteB = 0;
	}

	public double getSeiteB() 
	{
		return this.seiteB;
	}

	public double getDiagonale() 
	{
		return Math.sqrt((seiteA)*(seiteA) + (seiteB)*(seiteB));
	}

	public double getFlaeche() 
	{
		return seiteA*seiteB;
	}

	public double getUmfang() 
	{
		return 2*(seiteA + seiteB);
	}

	public void setMittelpunkt(Point mittelpunkt) 
	{
		this.mittelpunkt = mittelpunkt; 
	}

	public Point getMittelpunkt() 
	{
		return mittelpunkt;
	}
}
