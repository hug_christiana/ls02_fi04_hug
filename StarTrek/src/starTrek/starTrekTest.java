package starTrek;

public class starTrekTest
{

	public static void main(String[] args)
	{
		// Create Spaceship Enterprise 
		Spaceship spaceshipEnterprise = new Spaceship ("Federation", "Enterprise");

		// Set attributes for Enterprise
		spaceshipEnterprise.setEnergy_percent(100);
		spaceshipEnterprise.setDefenses_percent(100);
		spaceshipEnterprise.setHullResiliency_percent(100);
		spaceshipEnterprise.setLifeSupport_percent(100);
		spaceshipEnterprise.setPhotonTorpedoes_number(2);
		spaceshipEnterprise.setRepairAndroids_number(3);
		
		//Load freight onto Enterprise
		spaceshipEnterprise.addFreight("Equipment", "Tricorder", 120);
		spaceshipEnterprise.addFreight("Equipment", "Probe", 200);
		spaceshipEnterprise.addFreight("Weapon", "Torpedo", 3);
		
		//Print Enterprise status report, communication log and freight log
		spaceshipEnterprise.printStatusReport();
		spaceshipEnterprise.printCommunicationLog();
		spaceshipEnterprise.printFreightLog();
		
		System.out.println ("------");
		
		// Create Spaceship Borg 
		Spaceship spaceshipBorg = new Spaceship ("Borg", "Borg");
		
		// Set attributes for Borg ship
		spaceshipBorg.setEnergy_percent(120);
		spaceshipBorg.setDefenses_percent(100);
		spaceshipBorg.setHullResiliency_percent(100);
		spaceshipBorg.setLifeSupport_percent(100);
		spaceshipBorg.setPhotonTorpedoes_number(0);
		spaceshipBorg.setRepairAndroids_number(3);
		
		//Load freight onto Borg ship
		spaceshipBorg.addFreight("Creature", "Romulan", 300);
		spaceshipBorg.addFreight("Creature", "Vulcan", 50);
		spaceshipBorg.addFreight("Creature", "Klingon", 200);
		
		//Print Borg ship status report, communication log and freight log
		spaceshipBorg.printStatusReport();
		spaceshipBorg.printCommunicationLog();
		spaceshipBorg.printFreightLog();
		
		System.out.println ("------");

		spaceshipEnterprise.fireTorpedo(spaceshipBorg); //Fire 2nd torpedo at Borg ship
		spaceshipEnterprise.firePhaser(spaceshipBorg); //Fire 2nd phaser at Borg ship
		spaceshipEnterprise.printCommunicationLog(); //Get communication log for Enterprise
		spaceshipEnterprise.printStatusReport(); //Get new status report for Enteprise
		spaceshipBorg.printCommunicationLog(); //Get communication log for Enterprise
		spaceshipBorg.printStatusReport(); //Get new status report for Borg ship
		
		System.out.println ("------");
		
		spaceshipEnterprise.fireTorpedo(spaceshipBorg); //Fire 2nd torpedo at Borg ship
		spaceshipEnterprise.firePhaser(spaceshipBorg); //Fire 2nd phaser at Borg ship
		spaceshipEnterprise.printCommunicationLog(); //Get communication log for Enterprise
		spaceshipEnterprise.printStatusReport(); //Get new status report for Enterprise
		spaceshipBorg.printCommunicationLog(); //Get communication log for Enterprise
		spaceshipBorg.printStatusReport(); //Get new status report for Borg ship
		
		System.out.println ("------");
		
		spaceshipEnterprise.getFreightLogEntry(0).setQuantity(0); //reset quantity for tricorders to zero
		
		spaceshipEnterprise.clearFreightLog(); //clear tricorders from freight log
		spaceshipEnterprise.printFreightLog(); //print new freight log
		
		System.out.println ("------");
		
		spaceshipBorg.transmitRepairOrder(true, false, true, true, 4);

		spaceshipBorg.printStatusReport(); //check new status report
		
		System.out.println ("------");
		
		spaceshipEnterprise.loadTorpedoes(4);
		
		spaceshipEnterprise.printStatusReport();
		
		System.out.println ("------");

	}

}
