package starTrek;

import java.util.ArrayList;
import java.util.Random;

public class Spaceship
{
	private String type;
	private String name;
	private double energy_percent;
	private double defenses_percent;
	private double lifeSupport_percent;
	private double hullResiliency_percent;
	private int photonTorpedoes_number;
	private int repairAndroids_number;
	private ArrayList<String> broadcastCommunicator;
	private ArrayList<Freight> freightLog;

	/**
	* Class constructor specifying type of spaceship.
	* 
	* @param type	Specifies the type of spaceship ("Federation", "Klingon", "Romulan"
	* 				"Vulcan", "Borg")
	*/
	public Spaceship(String type)
	{
		setType(type);
		setName("unnamed");
		setEnergy_percent(0);
		setDefenses_percent(0);
		setLifeSupport_percent(0);
		setHullResiliency_percent(0);
		setPhotonTorpedoes_number(0);
		setRepairAndroids_number(0);
		broadcastCommunicator = new ArrayList<String>();
		freightLog = new ArrayList<Freight>();
	}
	
	/**
	* Class constructor specifying type and name of spaceship ("Federation", 
	* "Klingon", "Romulan", "Vulcan", "Borg").
	* 
	* @param type	Specifies the type of this spaceship ("Federation", "Klingon", 
	* 				"Romulan", "Vulcan", "Borg")
	* @param name 	Specifies the name of this spaceship
	*/
	public Spaceship(String type, String name)
	{
		setType(type);
		setName(name);
		setEnergy_percent(0);
		setDefenses_percent(0);
		setLifeSupport_percent(0);
		setHullResiliency_percent(0);
		setPhotonTorpedoes_number(0);
		setRepairAndroids_number(0);
		broadcastCommunicator = new ArrayList<String>();
		freightLog = new ArrayList<Freight>();
	}

	// Getters and setters
	
	/**
	* Gets the type for this spaceship.
	* 
	* @return Returns the type for this spaceship.
	*/
	public String getType()
	{
		return type;
	}
	
	/**
	* Sets the type for this spaceship. Checks for validity of type specified.
	* 
	* @param type	Specifies the type of this spaceship ("Federation", "Klingon", 
	* 				"Romulan", "Vulcan", "Borg")
	*/
	public void setType(String type)
	{
		if (type == "Federation"||type == "Klingon"||type == "Romulan"||
			type == "Vulcan"||type == "Borg")
		{
			this.type = type;
		}
		else {
			System.out.println ("Please specify valid ship type.");
		}
	}
	
	/**
	* Gets the name of this spaceship.
	* 
	* @return Returns the name of this spaceship.
	*/
	public String getName()
	{
		return name;
	}
	
	/**
	* Sets the name of this spaceship.
	* 
	* @param name	Specifies the type of this spaceship.
	*/
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	* Gets the energy level of this spaceship.
	* 
	* @return Returns the energy level of this spaceship.
	*/
	public double getEnergy_percent()
	{
		return energy_percent;
	}
	
	/**
	* Sets the energy level for this spaceship. Checks whether it is 0 or more,
	* if not sets it to 0.
	* 
	* @param energy_percent	Specifies the energy level for this spaceship.
	*/

	public void setEnergy_percent(double energy_percent)
	{
		if (energy_percent > 0)
			this.energy_percent = energy_percent;
		else
			this.energy_percent = 0;
	}
	
	/**
	* Gets the defenses level of this spaceship.
	* 
	* @return Returns the defenses level of this spaceship.
	*/
	public double getDefenses_percent()
	{
		return defenses_percent;
	}

	/**
	* Sets the defenses level for this spaceship. Checks whether it is 0 or more,
	* if not sets it to 0.
	* 
	* @param defenses_percent	Specifies the defenses level for this spaceship.
	*/
	public void setDefenses_percent(double defenses_percent)
	{
		if (defenses_percent > 0)
			this.defenses_percent = defenses_percent;
		else
			this.defenses_percent = 0;
	}

	/**
	* Gets the life support level of this spaceship.
	* 
	* @return Returns the life support level of this spaceship.
	*/
	public double getLifeSupport_percent()
	{
		return lifeSupport_percent;
	}

	/**
	* Sets the life support level for this spaceship. Checks whether it is 0 or more,
	* if not sets it to 0.
	* 
	* @param lifeSupport_percent	Specifies the life support level for this spaceship.
	*/
	public void setLifeSupport_percent(double lifeSupport_percent)
	{
		if (lifeSupport_percent > 0)
			this.lifeSupport_percent = lifeSupport_percent;
		else
			this.lifeSupport_percent = 0;
	}
	
	/**
	* Gets the hull resiliency level of this spaceship.
	* 
	* @return Returns the energy level of this spaceship.
	*/
	public double getHullResiliency_percent()
	{
		return hullResiliency_percent;
	}
	
	/**
	* Sets the hull resiliency level for this spaceship. Checks whether it is 0 or more,
	* if not sets it to 0.
	* 
	* @param hullResiliency_percent	Specifies the hull resiliency level for this spaceship.
	*/
	public void setHullResiliency_percent(double hullResiliency_percent)
	{
		if (hullResiliency_percent > 0)
			this.hullResiliency_percent = hullResiliency_percent;
		else
			this.hullResiliency_percent = 0;
	}

	/**
	* Gets the number of photon torpedoes currently loaded into this
	* ship's torpedo launcher.
	* 
	* @return Returns the number of photon torpedoes loaded into this ship's launcher.
	*/
	public int getPhotonTorpedoes_number()
	{
		return photonTorpedoes_number;
	}

	/**
	* Sets the number of photon levels loaded into this ship's launcher. 
	* Checks whether it is 0 or more, if not sets it to 0.
	* 
	* @param photonTorpedoes_number	Specifies the hull resiliency level for this spaceship.
	*/
	public void setPhotonTorpedoes_number(int photonTorpedoes_number)
	{
		if (photonTorpedoes_number > 0)
			this.photonTorpedoes_number = photonTorpedoes_number;
		else
			this.photonTorpedoes_number = 0;
	}

	/**
	* Gets the number of repair androids available on this ship.
	* 
	* @return Returns number of repair androids available on this ship.
	*/
	public int getRepairAndroids_number()
	{
		return repairAndroids_number;
	}

	/**
	* Sets the number of repair androids available on this spaceship. 
	* Checks whether it is 0 or more, if not sets it to 0.
	* 
	* @param repairAndroids_number	Specifies the hull resiliency level for this spaceship.
	*/
	public void setRepairAndroids_number(int repairAndroids_number)
	{
		if (repairAndroids_number > 0)
			this.repairAndroids_number = repairAndroids_number;
		else
			this.repairAndroids_number = 0;
	}

	/**
	* Gets the communication log entry specified. 
	* 
	* @param  index		Specifies the communicaton log entry's index number.
	* @return 			Returns message specified as a String.
	*/
	public String getCommunicationLogEntry(int index)
	{
		return this.broadcastCommunicator.get(index);
	}

	/**
	* Gets the freight log entry specified. 
	* 
	* @param  index		Specifies the freight log entry's index number.
	* @return Returns <code>Freight</code> specified.
	*/
	public Freight getFreightLogEntry(int index)
	{
		return this.freightLog.get(index);
	}

	// Methods
	
	/** 
	* Saves message in in this spaceship's communication log. 
	* 
	* @param message	Specifies the message to be saved in the communication log.
	*/
	public void transmitBroadcast(String message)
	{
		this.broadcastCommunicator.add(message);
	}
	
	/** 
	* Loads available additional torpedoes from the cargo hold
	* into this spaceship's torpedo launcher.
	* 
	* @param torpedoes	Specifies the number of torpedoes to be loaded from the cargo
	* 					hold into the launcher. If this number exceed the number in the cargo hold,
	* 					the available number of torpedoes will be loaded instead.
	*/
	public void loadTorpedoes (int torpedoes) {

		int torpedoesInCargo = 0;
		int photonTorpedoIndex = 0;
		boolean additionalTorpedoes = false;

		for (int i = 0; i < this.freightLog.size(); i++)
		{
			if (this.getFreightLogEntry(i).getName() == "Torpedo")
			{
				torpedoesInCargo = torpedoesInCargo + this.getFreightLogEntry(i).getQuantity();
				photonTorpedoIndex = i;
				additionalTorpedoes = true;
			}
		}
		
		// load additional torpedoes if available 
		if (additionalTorpedoes == true) 
		{

			if (torpedoesInCargo < torpedoes) {
				this.setPhotonTorpedoes_number(this.getPhotonTorpedoes_number() + torpedoesInCargo);
				int newTorpedoNumber = 0;
				this.freightLog.get(photonTorpedoIndex).setQuantity(newTorpedoNumber);
			}
			
			if (torpedoesInCargo >= torpedoes) {
				this.setPhotonTorpedoes_number(this.getPhotonTorpedoes_number() + torpedoes);
				int newTorpedoNumber = torpedoesInCargo - torpedoes;
				this.freightLog.get(photonTorpedoIndex).setQuantity(newTorpedoNumber);
			}

			if (torpedoesInCargo == 0) {
				System.out.println ("No torpedoes found.");
				this.transmitBroadcast("-=*Click*=-");
			}
		}

	}
	
	/** 
	* Fires a photon torpedo at the specified spaceship and records
	* a hit for the specified spaceship.
	* 
	* @param target	Specifies the target at which the torpedo is fired.
	*/
	public void fireTorpedo(Spaceship target)
	{
		
		//check if torpedoes loaded
		
		if (this.getPhotonTorpedoes_number() > 0)
		{
			recordHit(target);
			this.transmitBroadcast("Photon torpedo fired!");
			//set new number of torpedoes available
			this.setPhotonTorpedoes_number(this.getPhotonTorpedoes_number() -1 );
		} 
		else 
		{
			this.transmitBroadcast("-=*Click*=-");
			System.out.println("");
		}
	}
    
	/** 
	* Fires phaser at the specified spaceship and records a hit.
	* 
	* @param target	Specifies the target at which the phaser is fired.
	*/
	public void firePhaser(Spaceship target)
	{
		if (this.energy_percent > 49)
		{
			recordHit(target);
			this.transmitBroadcast("Phaser fired!");
			//reduce energy levels
			this.setEnergy_percent(this.energy_percent - 50);
		} 
		else
		{
			this.transmitBroadcast("-=*Click*=-");
			System.out.println("");
		}
	}
	
	/** 
	* Records a hit for the spaceship specified.
	* 
	* @param target	Specifies the target hit.
	*/
	public void recordHit(Spaceship target)
	{
		String targetName = target.getName();
		System.out.println(targetName + " hit!");
		System.out.println("");

		double targetDefenses = target.getDefenses_percent();
		double newTargetDefenses = targetDefenses - 50;
		target.setDefenses_percent(newTargetDefenses);

		if (newTargetDefenses <= 0)
		{
			double targetEnergy = target.getEnergy_percent();
			target.setEnergy_percent(targetEnergy - 50);

			double targetHull = target.getHullResiliency_percent();
			target.setHullResiliency_percent(targetHull - 50);
		}

		if (target.getHullResiliency_percent() <= 0)
		{
			target.setLifeSupport_percent(0);
			target.transmitBroadcast("Life support systems destroyed!");
		}
	}
	
	/** 
	* Loads new freight onto this spaceship and adds it to this
	* spacehip's freight log. Specifies the type, name and quantity of the 
	* new freight and creates a new Freight instance.
	* 
	* @param type		Specifies the type of freight ("Commodity", "Weapon", "Equipment").
	* @param name		Specifies the name of the freight.
	* @param quantity	Specifies the quantity of the freight to be loaded.
	*/
	public void addFreight(String type, String name, int quantity)
	{
		freightLog.add(new Freight(type, name, quantity));
	}
	
	/** 
	* Loads new freight onto the spaceship and adds it to the freight log.
	* Specifies the Freight instance.
	* 
	* @param freight	Specifies the <code>Freight</code> to be loaded onto 
	* 					this spaceship.
	*/
	public void addFreight(Freight freight)
	{
		freightLog.add(freight);
	}
	
	/** 
	* Prints this spaceship's status report for all attributes.
	*/
	public void printStatusReport()
	{
		System.out.println("Spaceship class: " + getType());
		System.out.println("Spaceship name: " + getName());
		System.out.println("Energy at " + getEnergy_percent() + "%");
		System.out.println("Defenses at " + getDefenses_percent() + "%");
		System.out.println("Life support at " + getLifeSupport_percent() + "%");
		System.out.println("Hull resiliency at " + getHullResiliency_percent() + "%");
		System.out.println("Photon torpedoes loaded: " + getPhotonTorpedoes_number());
		System.out.println("Repair androids available: " + getRepairAndroids_number());
		System.out.println("");
	}
	
	/** 
	* Prints this spaceship's communication log.
	*/
	public void printCommunicationLog()
	{
		System.out.println(this.getName() + " Broadcast Communication Log");
		if (this.broadcastCommunicator.size() > 0)
		{
			for (int i = 0; i < this.broadcastCommunicator.size(); i++)
			{
				String broadcastLogEntry = getCommunicationLogEntry(i);
				System.out.println("Message: " + broadcastLogEntry);
			}
			System.out.println("");
		} else
		{
			System.out.println("No log entries.");
			System.out.println("");
		}
	}
	
	/** 
	* Prints this spaceship's freight log.
	*/
	public void printFreightLog()
	{
		System.out.println(this.getName() + " Freight Log");
		if (this.freightLog.size() > 0)
		{
			for (int i = 0; i < this.freightLog.size(); i++)
			{
				Freight freightLogEntry = getFreightLogEntry(i);
				System.out.println(
						"Freight: " + freightLogEntry.getName() + "; Quantity: " + freightLogEntry.getQuantity());
			}
			System.out.println("");
		} else
		{
			System.out.println("No log entries.");
			System.out.println("");
		}
	}
	
	/** 
	* Checks this spaceship's freight log for empty freight and removes it
	* from the log.
	*/
	public void clearFreightLog()
	{
		if (this.freightLog.size() >= 0 )
		{
			for(int i = 0; i < this.freightLog.size(); i++)
			{
				if (this.getFreightLogEntry(i).getQuantity() == 0)
				{
					this.freightLog.remove(this.getFreightLogEntry(i));
				}
			}
			System.out.println("Freight log cleared.");
			System.out.println("");
		}
	}
	
	/** 
	* Transmits a repair order for this spacehip's specified attributes and number of 
	* repair androids.
	* 
	* @param repairDefenses 	Specifies whether to repair this ship's defenses.
	* @param repairHull 		Specifies whether to repair this ship's hull.
	* @param repairEnergy 		Specifies whether to repair this ship's energy levels.
	* @param repairLifeSupport 	Specifies whether to repair this ship's 
	* 							life support system.
	* @param androidsToUse		Specifies how many repair androids to use for repairs.
	* 							If the number specified is higher than the number of
	* 							androids available, all available androids will be
	* 							deployed.
	*/
	public void transmitRepairOrder (boolean repairDefenses, boolean repairHull, 
			boolean repairEnergy, boolean repairLifeSupport, int androidsToUse)
	{

		
		int deployedAndroids = 0;

		boolean[] repairsToDo = {repairDefenses, repairHull, repairEnergy,
				repairLifeSupport};

		int repairsToDoNumber = 0;
		for (int i = 0; i < repairsToDo.length; i++) 
		{
			if (repairsToDo[i] == true);
			{
				repairsToDoNumber++;
			}
		}
		
		int min = 1;
		int max = 100;

		Random random = new Random();

		int value = random.nextInt(max + min) + min;

		if (androidsToUse < this.getRepairAndroids_number())
		{
			deployedAndroids = this.getRepairAndroids_number();
		}
		else
		{
			deployedAndroids = androidsToUse;
		}

		for(int i = 0; i < this.freightLog.size(); i++)
		{
			Freight freightLogEntry = getFreightLogEntry(i);

			if (freightLogEntry.getQuantity() == 0)
			{
				freightLog.remove(freightLogEntry);
			}
		}

		double percentRepaired = (value * deployedAndroids) / repairsToDoNumber;

		if (repairDefenses == true) 
		{
			this.setDefenses_percent(this.getDefenses_percent()+percentRepaired);
			System.out.println("Defenses restored to " + this.getDefenses_percent() + "%");
		}

		if (repairHull == true) 
		{
			this.setHullResiliency_percent (this.getHullResiliency_percent()+percentRepaired);
			System.out.println("Hull resiliency restored to " + this.getHullResiliency_percent() + "%");
		}

		if (repairEnergy == true) 
		{
			this.setEnergy_percent(this.getEnergy_percent()+percentRepaired);
			System.out.println("Energy level restored to " + this.getEnergy_percent() + "%");
		}

		if (repairLifeSupport == true) 
		{
			this.setLifeSupport_percent(this.getLifeSupport_percent()+percentRepaired);
			System.out.println("Life support systems restored to " + this.getLifeSupport_percent() + "%");
		}
		
		System.out.println("");
	}
}

