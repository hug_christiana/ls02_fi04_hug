package starTrek;

public class Freight
{
	private String type; 
	private String name;
	private int quantity;
	
	/**
	* Class constructor specifying type and name of this freight.
	* 
	* @param type	Specifies the type of this freight ("Commodity", "Weapon", "Equipment")
	* @param name	Specifies the name of this freight.
	*/
	public Freight(String type, String name)
	{
		setType(type);
		setName(name);
		setQuantity(0);
	}

	/**
	* Class constructor specifying type, name and quantity of this freight.
	* 
	* @param type		Specifies the type of this freight ("Commodity", "Weapon", "Equipment")
	* @param name		Specifies the name of this freight.
	* @param quantity	Specifies the quantity of this freight.
	*/
	public Freight(String type, String name, int quantity)
	{
		setType(type);
		setName(name);
		setQuantity(quantity);
	}

	/**
	* Sets the type for this freight. Checks for validity of type specified.
	* 
	* @param type	Specifies the type of this spaceship ("Commodity", "Equipment", 
	* 				"Weapon")
	*/
	public void setType(String type)
	{
		if (type == "Commodity"||type == "Weapon"||type == "Equipment")
		{
			this.type = type;
		}
		else {
			System.out.println ("Please specify valid ship type.");
		}
	}

	/**
	* Gets the type of this spaceship.
	* 
	* @return Returns type of this spaceship.
	*/
	public String getType()
	{
		return type;
	}

	/**
	* Sets the name of this freight. 
	* 
	* @param name	Specifies the name of this freight.
	*/
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	* Gets the name of this freight.
	* 
	* @return Returns name of this freight.
	*/
	public String getName()
	{
		return name;
	}

	/**
	* Sets the quantity for this freight. Checks for validity of type specified.
	* Checks whether it is 0 or more, if not sets it to 0.
	* 
	* @param quantity	Specifies the quantity of this freight.
	*/
	public void setQuantity(int quantity)
	{
		if (quantity > 0)
			this.quantity = quantity;
		else 
			this.quantity = 0;
	}

	/**
	* Gets the quantity of this freight.
	* 
	* @return Returns the quantity of this freight.
	*/
	public int getQuantity()
	{
		return quantity;
	}

}
