package starTrek;

public class starTrek
{

	public static void main(String[] args)
	{
		//Instanzierung Raumschiffe
		
		Spaceship klingonen = new Spaceship ("Klingon", "IKS Hegh'ta");
		Spaceship romulaner = new Spaceship ("Romulan", "IRW Khazara");
		Spaceship vulkanier = new Spaceship ("Vulcan", "Ni'Var");
		
		//Attribute Raumschiffe setzen
		
		klingonen.setEnergy_percent(100);
		klingonen.setDefenses_percent(100);
		klingonen.setHullResiliency_percent(100);
		klingonen.setLifeSupport_percent(100);
		klingonen.setPhotonTorpedoes_number(1);
		klingonen.setRepairAndroids_number(2);
		
		romulaner.setEnergy_percent(100);
		romulaner.setDefenses_percent(100);
		romulaner.setHullResiliency_percent(100);
		romulaner.setLifeSupport_percent(100);
		romulaner.setPhotonTorpedoes_number(2);
		romulaner.setRepairAndroids_number(2);
		
		vulkanier.setEnergy_percent(80);
		vulkanier.setDefenses_percent(80);
		vulkanier.setHullResiliency_percent(50);
		vulkanier.setLifeSupport_percent(100);
		vulkanier.setPhotonTorpedoes_number(0);
		vulkanier.setRepairAndroids_number(5);
		
		//Instanziere Ladungen
		
		Freight ladungSchneckensaft = new Freight ("Commodity", "Ferengi Schneckensaft", 200);
		Freight ladungBorgSchrott = new Freight ("Commodity", "Borg Schrott", 5);
		Freight ladungRoteMaterie = new Freight ("Commodity", "Rote Materie", 2);
		Freight ladungForschungssonde = new Freight ("Equipment", "Forschungssonde", 35);
		Freight ladungSchwert = new Freight ("Weapon", "Bat'leth Klingonen Schwert", 200);
		Freight ladungPlasmaWaffe = new Freight ("Weapon", "Plasma-Waffe", 50);
		Freight ladungPhotonentorpedo = new Freight ("Weapon", "Torpedo", 3);
		
		//Verlade Ladungen in Schiffe
		
		klingonen.addFreight(ladungSchneckensaft);
		klingonen.addFreight(ladungSchwert);
		
		romulaner.addFreight(ladungBorgSchrott);
		romulaner.addFreight(ladungRoteMaterie);
		romulaner.addFreight(ladungPlasmaWaffe);
		
		vulkanier.addFreight(ladungForschungssonde);
		vulkanier.addFreight(ladungPhotonentorpedo);
		
		//Ausgabe Anfangszustand
		
		System.out.println ("Anfangszustand Klingonen:");
		System.out.println();
		klingonen.printStatusReport();
		klingonen.printCommunicationLog();
		klingonen.printFreightLog();
		
		System.out.println ("Anfangszustand Romulaner:");
		System.out.println();
		romulaner.printStatusReport();
		romulaner.printCommunicationLog();
		romulaner.printFreightLog();
		
		System.out.println ("Anfangszustand Vulkanier:");
		System.out.println();
		vulkanier.printStatusReport();
		vulkanier.printCommunicationLog();
		vulkanier.printFreightLog();
		
		System.out.println ("=======");
		
		//Aufruf Methoden
		
		klingonen.fireTorpedo(romulaner);
		romulaner.firePhaser(klingonen);
		
		vulkanier.transmitBroadcast("Gewalt ist nicht logisch.");
		
		klingonen.printStatusReport();
		klingonen.printFreightLog();
		
		vulkanier.transmitRepairOrder(true, true, true, true, 5);
		
		vulkanier.loadTorpedoes(4);
		vulkanier.clearFreightLog();
		
		klingonen.fireTorpedo(romulaner);
		klingonen.fireTorpedo(romulaner);
		
		
		System.out.println ("=======");
		
		//Ausgabe Endzustand
		
		System.out.println ("Endzustand Klingonen:");
		System.out.println();
		klingonen.printStatusReport();
		klingonen.printCommunicationLog();
		klingonen.printFreightLog();
		
		System.out.println ("Endzustand Romulaner:");
		System.out.println();
		romulaner.printStatusReport();
		romulaner.printCommunicationLog();
		romulaner.printFreightLog();
		
		System.out.println ("Endzustand Vulkanier:");
		System.out.println();
		vulkanier.printStatusReport();
		vulkanier.printCommunicationLog();
		vulkanier.printFreightLog();
		
		System.out.println ("=======");
	}

}
